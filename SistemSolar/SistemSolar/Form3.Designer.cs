﻿namespace SistemSolar
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.initializareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iesireCaractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDPlanetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numePlanetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rPlanetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gPlanetaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planeteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.planeteDataSet = new SistemSolar.PlaneteDataSet();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Caracteristici = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valori = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.Planeta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Caracteristica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valoare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.planeteTableAdapter = new SistemSolar.PlaneteDataSetTableAdapters.PlaneteTableAdapter();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planeteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planeteDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DarkCyan;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.initializareToolStripMenuItem,
            this.iesireCaractToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1191, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // initializareToolStripMenuItem
            // 
            this.initializareToolStripMenuItem.Name = "initializareToolStripMenuItem";
            this.initializareToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.initializareToolStripMenuItem.Text = "Initializare";
            this.initializareToolStripMenuItem.Click += new System.EventHandler(this.initializareToolStripMenuItem_Click);
            // 
            // iesireCaractToolStripMenuItem
            // 
            this.iesireCaractToolStripMenuItem.Name = "iesireCaractToolStripMenuItem";
            this.iesireCaractToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.iesireCaractToolStripMenuItem.Text = "Iesire->Caract.";
            this.iesireCaractToolStripMenuItem.Click += new System.EventHandler(this.iesireCaractToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1191, 465);
            this.splitContainer1.SplitterDistance = 318;
            this.splitContainer1.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDPlanetaDataGridViewTextBoxColumn,
            this.numePlanetaDataGridViewTextBoxColumn,
            this.rPlanetaDataGridViewTextBoxColumn,
            this.gPlanetaDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.planeteBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(318, 465);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // iDPlanetaDataGridViewTextBoxColumn
            // 
            this.iDPlanetaDataGridViewTextBoxColumn.DataPropertyName = "ID_Planeta";
            this.iDPlanetaDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDPlanetaDataGridViewTextBoxColumn.Name = "iDPlanetaDataGridViewTextBoxColumn";
            this.iDPlanetaDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDPlanetaDataGridViewTextBoxColumn.Width = 30;
            // 
            // numePlanetaDataGridViewTextBoxColumn
            // 
            this.numePlanetaDataGridViewTextBoxColumn.DataPropertyName = "Nume_Planeta";
            this.numePlanetaDataGridViewTextBoxColumn.HeaderText = "Nume Planeta";
            this.numePlanetaDataGridViewTextBoxColumn.Name = "numePlanetaDataGridViewTextBoxColumn";
            // 
            // rPlanetaDataGridViewTextBoxColumn
            // 
            this.rPlanetaDataGridViewTextBoxColumn.DataPropertyName = "R_Planeta";
            this.rPlanetaDataGridViewTextBoxColumn.HeaderText = "R Planeta";
            this.rPlanetaDataGridViewTextBoxColumn.Name = "rPlanetaDataGridViewTextBoxColumn";
            this.rPlanetaDataGridViewTextBoxColumn.Width = 80;
            // 
            // gPlanetaDataGridViewTextBoxColumn
            // 
            this.gPlanetaDataGridViewTextBoxColumn.DataPropertyName = "G_Planeta";
            this.gPlanetaDataGridViewTextBoxColumn.HeaderText = "G Planeta";
            this.gPlanetaDataGridViewTextBoxColumn.Name = "gPlanetaDataGridViewTextBoxColumn";
            this.gPlanetaDataGridViewTextBoxColumn.Width = 65;
            // 
            // planeteBindingSource
            // 
            this.planeteBindingSource.DataMember = "Planete";
            this.planeteBindingSource.DataSource = this.planeteDataSet;
            // 
            // planeteDataSet
            // 
            this.planeteDataSet.DataSetName = "PlaneteDataSet";
            this.planeteDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridView3);
            this.splitContainer2.Size = new System.Drawing.Size(869, 465);
            this.splitContainer2.SplitterDistance = 424;
            this.splitContainer2.TabIndex = 3;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Caracteristici,
            this.Valori,
            this.Unit});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(424, 465);
            this.dataGridView2.TabIndex = 0;
            // 
            // Caracteristici
            // 
            this.Caracteristici.HeaderText = "Caracteristici";
            this.Caracteristici.Name = "Caracteristici";
            this.Caracteristici.Width = 200;
            // 
            // Valori
            // 
            this.Valori.HeaderText = "Valori";
            this.Valori.Name = "Valori";
            // 
            // Unit
            // 
            this.Unit.HeaderText = "UM";
            this.Unit.Name = "Unit";
            this.Unit.Width = 80;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Planeta,
            this.Caracteristica,
            this.Valoare,
            this.UM});
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(441, 465);
            this.dataGridView3.TabIndex = 0;
            // 
            // Planeta
            // 
            this.Planeta.HeaderText = "Planeta";
            this.Planeta.Name = "Planeta";
            this.Planeta.Width = 85;
            // 
            // Caracteristica
            // 
            this.Caracteristica.HeaderText = "Caracteristica";
            this.Caracteristica.Name = "Caracteristica";
            this.Caracteristica.Width = 165;
            // 
            // Valoare
            // 
            this.Valoare.HeaderText = "Valoare";
            this.Valoare.Name = "Valoare";
            this.Valoare.Width = 75;
            // 
            // UM
            // 
            this.UM.HeaderText = "UM";
            this.UM.Name = "UM";
            this.UM.Width = 70;
            // 
            // planeteTableAdapter
            // 
            this.planeteTableAdapter.ClearBeforeFill = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 489);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form3";
            this.Text = "Administrare";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planeteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planeteDataSet)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem initializareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iesireCaractToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private PlaneteDataSet planeteDataSet;
        private System.Windows.Forms.BindingSource planeteBindingSource;
        private PlaneteDataSetTableAdapters.PlaneteTableAdapter planeteTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPlanetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numePlanetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rPlanetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gPlanetaDataGridViewTextBoxColumn;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Planeta;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caracteristica;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valoare;
        private System.Windows.Forms.DataGridViewTextBoxColumn UM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caracteristici;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valori;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
    }
}