﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace SistemSolar
{
    public partial class Form5 : Form
    {
        string cale = Application.StartupPath;
        PictureBox pictureBox2 = new PictureBox();
        Graphics g;
        int vala = 125, valb = 192, cra = 50, crb = 16;
        Pen p = new Pen(Color.WhiteSmoke);
        Panel[] pictures = new Panel[8];
        string[] imgs = new string[8] { "Mercur", "Venus", "Pamant", "Marte", "Jupiter", "Saturn", "Uranus", "Neptun" };
        uint[] sec = new uint[8] { 4167, 1639, 1000, 531, 84, 34, 12, 6 };
        //uint[] sec = new uint[8] { 9, 8, 7, 6, 5, 3, 2, 1 };
        int[] grade= new int[8];
        ulong progress=0;
        public Form5()
        {
            InitializeComponent();
            cale = cale.Substring(0, cale.Length - 10);
            cale = cale.Replace(@"\", "/");
        }

        private void incarcaback()
        {
            Image img = Image.FromFile(cale + "/Resurse/background.jpeg");
            //panel1.BackgroundImage = img;
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            incarcaback();

            Image soare = Image.FromFile(cale + "/Resources/soare.gif-c200");
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.Image = soare;
            pictureBox2.Height = 50;
            pictureBox2.Width = 54;
            pictureBox2.Location = new Point((panel1.ClientSize.Width / 2) - (pictureBox2.Width / 2), (panel1.ClientSize.Height / 2) - (pictureBox2.Height / 2));

            panel1.Controls.Add(pictureBox2);

            planete();
        }

        private void orbita()
        {
            int x = panel1.ClientSize.Width;
            int y = panel1.ClientSize.Height;
            int a=10,b=80;
            g = panel1.CreateGraphics();
            for (int i = 0; i < 8; i++)
            {
                g.DrawEllipse(p, a, b, x - 2 * a, y - 2 * b);
                a = a + 50;
                b = b + 16;
            }
        }

        private void planete()
        {
            int a = 125,d=17;
            int y = panel1.ClientSize.Height;
            int x = panel1.ClientSize.Width;
            for (int i = 0; i < 8; i++)
            {
                Image img = Image.FromFile(cale + "/Resources/" + imgs[i] + ".png");

                pictures[i] = new Panel();
                pictures[i].BackgroundImageLayout = ImageLayout.Stretch;
                pictures[i].BackgroundImage = img;
                pictures[i].Height = d;
                if (i == 5)
                {
                    d += 30; a -= 15;
                    pictures[i].Width = d;
                    d -= 30;
                    pictures[i].Tag = d;
                    pictures[i].Location = new Point(x / 2 + a, y / 2 - d / 2);
                    a += 15;
                }
                else
                {
                    pictures[i].Width = d;
                    pictures[i].Tag = d;
                    pictures[i].Location = new Point(x / 2 + a, y / 2 - d / 2);
                }
                panel1.Controls.Add(pictures[i]);
                a = a + 50;
                d = d + 2;
            }
        }

        private void misca(int i)
        {
            grade[i] += 1;
            double x = (vala + i * cra + int.Parse(pictures[i].Tag.ToString()) / 2) * Math.Cos(grade[i] * Math.PI / 180);
            double y = (225-valb + i * crb) * Math.Sin(grade[i] * Math.PI / 180);
            int a = Convert.ToInt16(x + panel1.ClientSize.Width / 2 - pictures[i].ClientSize.Width / 2);
            int b = Convert.ToInt16(y + panel1.ClientSize.Height / 2 - pictures[i].ClientSize.Height / 2);
            pictures[i].Location = new Point(a, b);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progress++;
            if (progress % sec[7] == 0)
                //mercur
                misca(0);
            if (progress % sec[6] == 0)
                //venus
                misca(1);
            if (progress % sec[5] == 0)
                //pamant
                misca(2);
            if (progress % sec[4] == 0)
                //marte
                misca(3);
            if (progress % sec[3] == 0)
                //jupiter
                misca(4);
            if (progress % sec[2] == 0)
                //saturn
                misca(5);
            if (progress % sec[1] == 0)
                //uranus
                misca(6);
            if (progress % sec[0] == 0)
                //neptun
                misca(7);
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            orbita();
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }
    }
}
