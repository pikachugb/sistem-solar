﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;

namespace SistemSolar
{
    public partial class Form1 : Form
    {
        SqlConnection cn;
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        Boolean ok,admin;
        public Form1()
        {
            InitializeComponent();
            string cale = Application.StartupPath;
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS2008;AttachDbFilename="+cale+@"\DBSistem.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
            textBox2.PasswordChar = '*';
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void sf2()
        {
            Application.Run(new Form2());
        }

        public static class Globals
        {
            public static bool admin { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cn.Open();
            cmd.CommandText = "Select Nume_Utilizator,Parola,Email from Utilizatori where Nume_Utilizator='" + textBox1.Text + "' and Parola='" + textBox2.Text + "' and Email='" + textBox3.Text + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                ok = true;
            else
                ok = false;
            if (ok)
            {
                if (textBox1.Text == "Administrator")
                    Globals.admin = true;
                else
                    Globals.admin = false;
                Thread t = new Thread(new ThreadStart(sf2));
                t.Start();
                this.Close();
            }
            else
                MessageBox.Show("Numele de utilizator, parola sau adresa de email sunt incorecte!");
            cn.Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1.PerformClick();
        }
    }
}
