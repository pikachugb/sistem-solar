﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace SistemSolar
{
    public partial class Form4 : Form
    {
        SqlCommand cmd = new SqlCommand(), cmd1 = new SqlCommand();
        SqlConnection cn, cn1;
        SqlDataReader dr, dr1;
        Point[] p = new Point[6] { new Point(37, 59), new Point(37, 104), new Point(37, 149), new Point(37, 194), new Point(37, 239), new Point(37, 286) };
        Boolean[] n = new Boolean[4] { false, false, false, false };
        int[] carac = new int[1000];
        string cale = Application.StartupPath;
        int lista = 0,nrcarac=0;
        int x = 375, y = 120, z = 50;
        Font myFont = new Font("Aerial", 10, FontStyle.Regular);

        public Form4()
        {
            InitializeComponent();
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS2008;AttachDbFilename=" + cale + @"\DBSistem.mdf;Integrated Security=True;User Instance=True");
            cn1 = new SqlConnection(@"Data Source=.\SQLEXPRESS2008;AttachDbFilename=" + cale + @"\DBSistem.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
            cmd1.Connection = cn1;
        }

        private void inchidereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(sf2));
            t.Start();
            this.Close();
        }

        private void sf2()
        {
            Application.Run(new Form2());
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            cn.Open();
            cmd.CommandText = "select Nume_planeta from Planete";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                    comboBox1.Items.Add(dr[0].ToString());
            cn.Close();
            cale = cale.Replace(@"\", "/");
            Image img = Image.FromFile(cale + "/Resurse/Background.jpeg");
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
            panel1.BackgroundImage = img;
        }

        private void inserttab(GroupBox d, string g)
        {
            int idcar = 1,tipcar=1;
            cn.Open();
            cmd.CommandText = "select tip_caracteristica from Caracteristici where ID_caracteristica = '" + d.Tag + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                    tipcar = int.Parse(dr[0].ToString());
            cn.Close();
            cn.Open();
            cmd.CommandText = "Insert into Caracteristici(Denumire,Tip_caracteristica,UM) values ('" + d.Text + "','" + tipcar + "','" + null + "')";
            cmd.ExecuteNonQuery();
            cn.Close();
            cn.Open();
            cmd.CommandText = "Select ID_Caracteristica from Caracteristici";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                    idcar = int.Parse(dr[0].ToString());
            cn.Close();
            cn.Open();
            cmd.CommandText = "Insert into Valori(ID_planeta,ID_caracteristica,Valoare) values ('" + (comboBox1.SelectedIndex + 1).ToString() + "','" + idcar + "','" + g + "')";
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        private void updatetab(GroupBox d, string g)
        {
            cn.Open();
            cmd.CommandText = "Update Valori set Valoare='" + g + "' where ID_Caracteristica='" + d.Tag + "'";
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (n[0] == true && groupBox1.Visible ==true)
            {
                inserttab(groupBox1, dateTimePicker1.Value.Date.ToString("dd.MM.yyyy"));
            }
            else
            {
                updatetab(groupBox1, dateTimePicker1.Value.Date.ToString("dd.MM.yyyy"));
            }
            if (n[1] == true && groupBox2.Visible == true)
            {
                inserttab(groupBox2, textBox1.Text);
            }
            else
            {
                updatetab(groupBox2, textBox1.Text);
            }
            if (n[2] == true && groupBox3.Visible == true)
            {
                inserttab(groupBox3, numericUpDown1.Value.ToString());
            }
            else
            {
                updatetab(groupBox3, numericUpDown1.Value.ToString());
            }
            if (n[3] == true && groupBox4.Visible == true)
            {
                inserttab(groupBox4, numericUpDown2.Value.ToString());
            }
            else
            {
                updatetab(groupBox4, numericUpDown2.Value.ToString());
            }
            MessageBox.Show("Salvarea a fost realizata cu succes!!");
        }

        private void revenire()
        {
            groupBox1.Visible = false;
            groupBox2.Visible = false;
            groupBox3.Visible = false;
            groupBox4.Visible = false;
            button3.Visible = false;
        }

        private void aliniere()
        {
            for (int i = 0; i < 4; i++)
            {
                if (dr1[0].ToString() == "Data descoperirii")
                {
                    dateTimePicker1.Value = Convert.ToDateTime(dr[1].ToString());
                    groupBox1.Visible = true;
                    groupBox1.Location = p[lista];
                    groupBox1.Tag = int.Parse(dr[0].ToString());
                    button3.Visible = true;
                    groupBox5.Visible = true;
                    groupBox5.Location = p[lista + 1];
                    button3.Visible = true;
                    button3.Location = p[lista + 2];
                    y = y + z;
                    this.Size = new System.Drawing.Size(x, y);
                    lista++;
                    break;
                }
                if (dr1[0].ToString() == "Densitatea")
                {
                    textBox1.Text = dr[1].ToString();
                    groupBox2.Visible = true;
                    groupBox2.Location = p[lista];
                    groupBox2.Tag = int.Parse(dr[0].ToString());
                    button3.Visible = true;
                    groupBox5.Visible = true;
                    groupBox5.Location = p[lista + 1];
                    button3.Visible = true;
                    button3.Location = p[lista + 2];
                    y = y + z;
                    this.Size = new System.Drawing.Size(x, y);
                    lista++;
                    break;
                }
                if (dr1[0].ToString() == "Inclinatia aproximativa a orbitei")
                {
                    numericUpDown1.Value = int.Parse(dr[1].ToString());
                    groupBox3.Visible = true;
                    groupBox3.Location = p[lista];
                    groupBox3.Tag = int.Parse(dr[0].ToString());
                    groupBox5.Visible = true;
                    groupBox5.Location = p[lista + 1];
                    button3.Visible = true;
                    button3.Location = p[lista + 2];
                    y = y + z;
                    this.Size = new System.Drawing.Size(x, y);
                    lista++;
                    break;
                }
                if (dr1[0].ToString() == "Temperatura la suprafata")
                {
                    numericUpDown2.Value = int.Parse(dr[1].ToString());
                    groupBox4.Visible = true;
                    groupBox4.Location = p[lista];
                    groupBox4.Tag = int.Parse(dr[0].ToString());
                    button3.Visible = true;
                    groupBox5.Visible = true;
                    groupBox5.Location = p[lista + 1];
                    button3.Visible = true;
                    button3.Location = p[lista + 2];
                    y = y + z;
                    this.Size = new System.Drawing.Size(x, y);
                    lista++;
                    break;
                }
            }
        }

        private void fillcomboBox2()
        {
            comboBox2.Items.Clear();
            if (groupBox1.Visible == false)
                comboBox2.Items.Add(groupBox1.Text);
            if (groupBox2.Visible == false)
                comboBox2.Items.Add(groupBox2.Text);
            if (groupBox3.Visible == false)
                comboBox2.Items.Add(groupBox3.Text);
            if (groupBox4.Visible == false)
                comboBox2.Items.Add(groupBox4.Text);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            x = 375; y = 120;
            revenire();
            lista = 0;
            this.Size = new System.Drawing.Size(x, y);
            int id = 1;
            cn.Open();
            cmd.CommandText = "Select ID_Planeta from Planete where Nume_Planeta='" + comboBox1.SelectedItem.ToString() + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                {
                    id = int.Parse(dr[0].ToString());
                }
            cn.Close();
            cn.Open();
            cmd.CommandText = "Select ID_Caracteristica,Valoare from Valori where ID_Planeta='" + id + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                {
                    cn1.Open();
                    cmd1.CommandText = "Select Denumire,UM from Caracteristici where ID_Caracteristica = '" + int.Parse(dr[0].ToString()) + "'";
                    dr1 = cmd1.ExecuteReader();
                    if (dr1.HasRows)
                        while (dr1.Read())
                        {
                            aliniere();
                            this.Size = new Size(x, y + 85);
                        }
                    else
                    {
                        this.Size = new Size(x, y);
                    }
                    cn1.Close();
                }
            else
            {
                groupBox5.Visible = true;
                groupBox5.Location = p[0];
                button3.Visible = true;
                button3.Location = p[1];
                this.Size = new Size(x, y + z + 40);
            }
            cn.Close();
            y = y + 85;
            fillcomboBox2();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem.ToString() == "Data descoperirii"&&groupBox1.Visible==false)
            {
                n[0] = true;
                groupBox1.Visible = true;
                groupBox1.Location = p[lista];
                button3.Visible = true;
                groupBox5.Visible = true;
                groupBox5.Location = p[lista + 1];
                button3.Visible = true;
                button3.Location = p[lista + 2];
                y = y + z;
                this.Size = new System.Drawing.Size(x, y);
                lista++;
            }
            if (comboBox2.SelectedItem.ToString() == "Densitatea" && groupBox2.Visible == false)
            {
                n[1] = true;
                groupBox2.Visible = true;
                groupBox2.Location = p[lista];
                button3.Visible = true;
                groupBox5.Visible = true;
                groupBox5.Location = p[lista + 1];
                button3.Visible = true;
                button3.Location = p[lista + 2];
                y = y + z;
                this.Size = new System.Drawing.Size(x, y);
                lista++;
            }
            if (comboBox2.SelectedItem.ToString() == "Inclinatia aproximativa a orbitei" && groupBox3.Visible == false)
            {
                n[2] = true;
                groupBox3.Visible = true;
                groupBox3.Location = p[lista];
                groupBox5.Visible = true;
                groupBox5.Location = p[lista + 1];
                button3.Visible = true;
                button3.Location = p[lista + 2];
                y = y + z;
                this.Size = new System.Drawing.Size(x, y);
                lista++;
            }
            if (comboBox2.SelectedItem.ToString() == "Temperatura la suprafata" && groupBox4.Visible == false)
            {
                n[3] = true;
                groupBox4.Visible = true;
                groupBox4.Location = p[lista];
                button3.Visible = true;
                groupBox5.Visible = true;
                groupBox5.Location = p[lista + 1];
                button3.Visible = true;
                button3.Location = p[lista + 2];
                y = y + z;
                this.Size = new System.Drawing.Size(x, y);
                lista++;
            }
        }
    }
}
