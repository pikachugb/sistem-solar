﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Threading;

namespace SistemSolar
{
    public partial class Form3 : Form
    {
        SqlConnection cn,cn1;
        SqlCommand cmd=new SqlCommand(),cmd1 = new SqlCommand();
        SqlDataReader dr,dr1;
        string cale = Application.StartupPath;
        string[] planets = new string[] { "Mercur", "Venus", "Pamant", "Marte", "Jupiter", "Saturn", "Uranus", "Neptun" };
        public Form3()
        {
            InitializeComponent();
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS2008;AttachDbFilename=" + cale + @"\DBSistem.mdf;Integrated Security=True;User Instance=True");
            cn1 = new SqlConnection(@"Data Source=.\SQLEXPRESS2008;AttachDbFilename=" + cale + @"\DBSistem.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
            cmd1.Connection = cn1;
        }

        private void depopulare()
        {
            cn.Open();
            cmd.CommandText = "Truncate table Planete";
            cmd.ExecuteNonQuery();
            cn.Close();
            
            cn.Open();
            cmd.CommandText = "Truncate table Valori";
            cmd.ExecuteNonQuery();
            cn.Close();

            cn.Open();
            cmd.CommandText = "Truncate table Caracteristici";
            cmd.ExecuteNonQuery();
            cn.Close();
        }
        private void planete()
        {
            cale.Replace(@"\","/");
            StreamReader reader = new StreamReader(cale+"/Resurse/planete.txt");
            string line, separator = ", ";
            string[] date = new string[5];
            while ((line = reader.ReadLine()) != null)
            {
                date = line.Split(new[] { separator }, StringSplitOptions.None);
                date[2]=date[2].Replace(',', '.');
                cn.Open();
                cmd.CommandText = "Insert into Planete(Nume_Planeta,R_Planeta,G_Planeta) values ('" + date[0] + "','" + date[1] + "','" + Convert.ToDouble(date[2]) + "')";
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
        private void caractval()
        {
            cale.Replace(@"\", "/");
            StreamReader reader = new StreamReader(cale + "/Resurse/Caracteristici.txt");
            string line, separator = " ";int idp=0,idc=0;
            string[] date = new string[10];
            while ((line = reader.ReadLine()) != null)
            {
                date = line.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries);
                cn.Open();
                cmd.CommandText = "Select ID_Planeta from Planete where Nume_Planeta='" + date[0] + "'";
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                        idp = int.Parse(dr[0].ToString());
                cn.Close();
                string den = date[1].Replace('_', ' ');
                cn.Open();
                cmd.CommandText = "Insert into Caracteristici(Denumire,Tip_caracteristica,UM) values ('" + den + "','" + int.Parse(date[2]) + "','" + date[3] + "')";
                cmd.ExecuteNonQuery();
                cn.Close();
                cn.Open();
                cmd.CommandText = "Select ID_Caracteristica from Caracteristici where Denumire = '" + den + "' and Tip_caracteristica='" + int.Parse(date[2]) + "' and UM='" + date[3] + "'";
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    while (dr.Read())
                        idc = int.Parse(dr[0].ToString());
                cn.Close();
                cn.Open();
                cmd.CommandText = "Insert into Valori(ID_Planeta,ID_Caracteristica,Valoare) values ('" + idp + "','" + idc + "','" + date[4] + "')";
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        private void initializareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            depopulare();
            planete();
            caractval();
            MessageBox.Show("Initializarea a fost facuta cu succes!");
        }

        private void filldata3()
        {
            cn.Open();
            cmd.CommandText = "Select ID_Caracteristica,Denumire,Tip_caracteristica,UM from Caracteristici";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                {
                    string den = dr[1].ToString().Replace('_', ' ');
                    string um = dr[3].ToString().Replace("NULL", " ");
                    cn1.Open();
                    cmd1.CommandText = "Select ID_planeta,Valoare from Valori where ID_Caracteristica='" + int.Parse(dr[0].ToString()) + "'";
                    dr1 = cmd1.ExecuteReader();
                    if (dr1.HasRows)
                    {
                        dr1.Read();
                        dataGridView3.Rows.Add(planets[int.Parse(dr1[0].ToString()) - 1], den, dr1[1].ToString(), um);
                    }
                    cn1.Close();
                }
            cn.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'planeteDataSet.Planete' table. You can move, or remove it, as needed.
            this.planeteTableAdapter.Fill(this.planeteDataSet.Planete);
            filldata3();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView2.Rows.Clear();
            cn.Open();
            cmd.CommandText = "Select ID_Caracteristica,Valoare from Valori where ID_Planeta='" + int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString()) + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                {
                    cn1.Open();
                    cmd1.CommandText = "Select Denumire,UM from Caracteristici where ID_Caracteristica='" + int.Parse(dr[0].ToString()) + "'";
                    dr1 = cmd1.ExecuteReader();
                    if (dr1.HasRows)
                        while (dr1.Read())
                        {
                            dataGridView2.Rows.Add(dr1[0].ToString(), dr[1].ToString(), dr1[1].ToString());
                        }
                    cn1.Close();
                }
            cn.Close();
        }

        private void iesireCaractToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(sf4));
            t.Start();
            this.Close();
        }

        private void sf4()
        {
            Application.Run(new Form4());
        }
    }
}

